//
//  Header.h
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/06.
//

#ifndef BoidCircle_h
#define BoidCircle_h

#include "ofMain.h"
#include "BaseScene.hpp"
class BoidCircle{
private:
    ofPoint vel = ofPoint(ofRandom(-1, 1), ofRandom(-1, 1));//速度(増加量)
    ofPoint accel = ofPoint(0, 0);                          //加速度
    float max_force = 0.03f;                                //最大運動量
    float max_speed = 2;                                    //最大速度
    float radius = 10;                                      //半径
    
//-------------------------------------------------------------
//一定範囲内に他のBoidCircleがあった場合、その複数のBoidCircleの反対方向の向き平均を計算する
    ofPoint separate(vector<BoidCircle> boids){
        int _count = 0;
        ofPoint sum = ofPoint(0);
        int threshold_area  = radius * 2;
        for(int i = 0; i < boids.size(); i++){
            float dist = pos.distance(boids[i].pos);
            if(dist > 0 && dist < threshold_area){
                ofPoint diff = pos - boids[i].pos;
                diff.normalize();
                diff /= dist;   //距離によって重み付けする
                sum += diff;
                _count++;
            }
        }
        if(_count > 0){
            sum = sum / (float)_count;
            sum.normalize();
            sum *= max_speed;
            sum -= vel;
            sum.limit(max_force);
        }
        return sum;
    }
    
//-------------------------------------------------------------
//速度の平均を計算して自身に適用する
    ofPoint align(vector<BoidCircle> boids){
        int _count = 0;
        ofPoint sum = ofPoint(0);
        int threshold_area  = radius * 4;
        for(int i = 0; i < boids.size(); i++){
            float dist = pos.distance(boids[i].pos);
            if(dist > 0 && dist < threshold_area){
                sum += boids[i].vel;
                _count++;
            }
        }
        if(_count > 0){
            sum = sum / (float)_count;
            sum.normalize();
            sum *= max_speed;
            ofPoint steer = (sum - vel).limit(max_force);
            return steer;
        }
        else{
            return ofPoint(0);
        }
    }
    
//-------------------------------------------------------------
//位置の平均を計算する
    ofPoint cohesion(vector<BoidCircle> boids){
        int _count = 0;
        ofPoint sum = ofPoint(0);
        int threshold_area  = radius * 4;
        for(int i = 0; i < boids.size(); i++){
            float dist = pos.distance(boids[i].pos);
            if(dist > 0 && dist < threshold_area){
                sum += boids[i].pos;
                _count++;
            }
        }
        if(_count > 0){
            sum = sum / (float)_count;
            ofPoint desired = (sum - pos).normalize() * max_speed;
            ofPoint steer = desired - vel;
            steer.limit(max_force);
            return steer;
        }
        else return ofPoint(0);
    }
    
public:
    ofPoint pos = ofPoint(ofRandom(SCREEN_W), ofRandom(SCREEN_H));
    void setMaxForce(float max_force){this-> max_force = max_force;}
//-------------------------------------------------------------
    void setMaxSpeed(float max_speed){this->max_speed = max_speed;}
//-------------------------------------------------------------
    void setRadius(float radius){this->radius = radius;}
//-------------------------------------------------------------
    void flock(vector<BoidCircle> boids){
        //Boidsの三原則
        //1. 周りと一定の距離を保つ
        accel += separate(boids) * 1.5f;
        //2.　周りと同じ方向に
        accel += align(boids) * 1.0f;
        //3. 近隣の中心に向かう
        accel += cohesion(boids) * 1.0f;
    }
    
//-------------------------------------------------------------
    void update(){
        vel += accel;
        vel.limit(max_speed);
        pos += vel;
        accel *= 0;//初期化
        
        if(pos.x < 0){
            vel.x *= -1;
            pos.x = 0;
        }
        else if(pos.x > SCREEN_W){
            vel.x *= -1;
            pos.x = SCREEN_W;
        }
        if(pos.y < 0){
            vel.y *= -1;
            pos.y = 0;
        }
        else if(pos.y > SCREEN_H){
            vel.y *= -1;
            pos.y = SCREEN_H;
        }
    }
    
//-------------------------------------------------------------
    void draw(){
        ofDrawCircle(pos, radius);
    }
};

#endif /* BoidCircle_h */
