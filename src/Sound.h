//
//  Sound.h
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/06.
//

#ifndef Sound_h
#define Sound_h

#include "ofMain.h"

#define SAMPLE_RATE 44100

//SoundManagerで音の数だけインスタンス化して使用
class Sound{
public:
    vector<float> buffer;       //最終的にScene000で線を描画するために使用
    float hz = 1000;            //現在の周波数
    float hz_target = 1000;     //ターゲットの周波数。音を鳴らす際は、まずこの周波数の値を変更する
    float volume = 1.0;         //音量。
    double phase = 0;           //現在の波形?位相?のx軸( 0 ~ phaseStep * N )
    float phaseStep;            //位相xの増加ステップ数。周波数 / サンプリング周波数 * 2PI
    
//-------------------------------------------------------------
    //phaseStepの更新。精度ずれが起きないようphaseの値を -2PI ~ 2PIの範囲内に収める
    void update(){
        phaseStep = hz / (float)SAMPLE_RATE * TWO_PI;
        while (phase > TWO_PI){
            phase -= TWO_PI;
        }
    }
};

//==================================================================

class SoundManager{
private:
//    ofMutex soundMutex;         //TODO : エラー防止用。詳しく調べる必要あり

public:
    int channel_num = 2;        //チャンネル数
    vector<Sound> sounds;       //SoundManagerで音の数だけインスタンス化して使用

//-------------------------------------------------------------
    void setup(){
//        ofSoundStreamListDevices ();
    }
    
//-------------------------------------------------------------
    void addMultiSounds(int _num){
        if(sounds.size() > 0){
            //数を増やす場合
            if(_num > sounds.size()){
                for(int i = sounds.size(); i < _num; i++){
                    sounds.push_back(Sound());
                }
            }
            else{//数を減らす場合
                for(int i = sounds.size(); i > _num; i--){
                    sounds.pop_back();
                    //TODO : delete
                }
            }
        }
        else{//新たに設定する場合
            for(int i = 0; i < _num; i++){
                sounds.push_back(Sound());
            }
        }
    }
    
//-------------------------------------------------------------
    void update(){
//        ofScopedLock soundLock(soundMutex);
        for(int i = 0; i < sounds.size(); i++){
            sounds[i].update();
        }
    }
    
//-------------------------------------------------------------
    ofSoundBuffer autioOut(ofSoundBuffer outBuffer){
//        ofScopedLock soundLock(soundMutex);
        ofSoundBuffer buf = outBuffer;
        for(int h = 0; h < sounds.size(); h++){
            sounds[h].buffer.resize(buf.size());
            for(int i = 0; i < outBuffer.size(); i+= channel_num){
                sounds[h].phase += sounds[h].phaseStep;
                for(int j = 0; j < channel_num; j++){
                    buf[i + j] += sin(sounds[h].phase) * sounds[h].volume *0.9;
                }
            }
        }
        for(int i = 0; i < buf.size(); i+= channel_num){
            for(int j = 0; j < channel_num; j++){
                buf[i + j] /= sounds.size();
                for(int h = 0; h < sounds.size(); h++){
                    sounds[h].buffer[i + j]  = buf[i + j];

                }
            }
        }
        return buf;
    }
    
//-------------------------------------------------------------
    void exit(){
//        ofScopedLock soundLock(soundMutex);
        for(int h = 0; h < sounds.size(); h++){
            sounds[h].buffer.clear();
        }
    }
};

#endif /* Sound_h */
