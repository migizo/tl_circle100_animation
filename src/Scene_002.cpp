//
//  Scene_002.cpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//

#include "Scene_002.hpp"
//--------------------------------------------------------------
void Scene_002::setup(){
    setParams();
    ofSetCylinderResolution(32, 2);
    font.load(font_filename, 48);
}
//--------------------------------------------------------------

void Scene_002::update(){
    int pattern_num = patterns.size()/4;
    if(ofGetFrameNum() % 60 == 0){
        current_pattern_idx = current_pattern_idx > pattern_num - 1 ? 0 : current_pattern_idx + 1;
    }
    float seed = fmodf(ofGetFrameNum(), 60) / 60.0f;
    
    m1 = ofMap(seed, 0, 1,
              patterns[current_pattern_idx],
              (current_pattern_idx + 1 > pattern_num - 1) ? patterns[0] :  patterns[current_pattern_idx + 1]);
    m2 = ofMap(seed, 0, 1,
               patterns[current_pattern_idx],
               (current_pattern_idx + 1 > pattern_num - 1) ? patterns[0] :  patterns[current_pattern_idx + 1]);
    n1 = ofMap(seed, 0, 1,
               patterns[current_pattern_idx + 1],
               (current_pattern_idx + 1 > pattern_num - 1) ? patterns[1] :  patterns[current_pattern_idx + 1 + 1]);
    n2 = ofMap(seed, 0, 1,
               patterns[current_pattern_idx + 2],
               (current_pattern_idx + 1 > pattern_num - 1) ? patterns[2] :  patterns[current_pattern_idx + 1 + 2]);
    n3 = ofMap(seed, 0, 1,
               patterns[current_pattern_idx + 3],
               (current_pattern_idx + 1 > pattern_num - 1) ? patterns[3] :  patterns[current_pattern_idx + 1 + 3]);
    
}
//--------------------------------------------------------------
void Scene_002::draw(){
    ofSetColor(0, 100, 255, alpha - 100);
    ofDrawRectangle(0, 0, SCREEN_W, SCREEN_H);

    cam.setPosition(0, 300, 0);
    cam.setTarget(ofPoint(0));
    cam.begin();
    
    int shape_num = 1;
    int div_num = 100 / shape_num;
    float step = TWO_PI / div_num;
    for(float i = 0; i < TWO_PI; i+= step){
        float _radius = superformula(i + ofGetElapsedTimef()/10, a, b, m1, m2, n1, n2, n3) * scale;
        ofPoint pos = pol2car(_radius, i);
        ofSetColor(255, 255, 255, alpha);

        ofDrawCylinder(pos.x, pos.z, pos.y, radius, thin);
    }
    
    cam.end();
    
//    //時間表示
//    ofSetColor(255, 0, 0, alpha);
//    font.drawString(ofGetTimestampString("%H:%M"), ofGetWidth() / 2 - font.getGlyphBBox().getWidth()/2, ofGetHeight() / 2 - font.getGlyphBBox().getHeight()/2);
}

//--------------------------------------------------------------
void Scene_002::setParams(){
    params.add(a.set("a", a, -100, 100));
    params.add(b.set("b", b, -100, 100));
    params.add(m1.set("m1", m1, -100, 100));
    params.add(m2.set("m2", m2, -100, 100));
    params.add(n1.set("n1", n1, -100, 100));
    params.add(n2.set("n2", n2, -100, 100));
    params.add(n3.set("n3", n3, -100, 100));
    params.add(radius.set("radius", radius, 1, 20));
    params.add(thin.set("thin", thin, 0, 20));
    params.add(scale.set("scale", scale, 1, 100));
    params.add(bg_color.set("bg_color", bg_color, ofColor(0), ofColor(255)));
    params.add(color.set("color", color, ofColor(0), ofColor(255)));
    params.add(outline_color.set("outline_color", outline_color, ofColor(0), ofColor(255)));
    params.add(line_color.set("line_color", line_color, ofColor(0), ofColor(255)));


}

//--------------------------------------------------------------
void Scene_002::load(){
    ofxXmlSettings xml;
    xml.loadFile(xml_filename);

    a = xml.getValue("a", a);
    b = xml.getValue("b", b);
    m1 = xml.getValue("m1", m1);
    m2 = xml.getValue("m2", m2);
    n1 = xml.getValue("n1", n1);
    n2 = xml.getValue("n2", n2);
    n3 = xml.getValue("n3", n3);
    radius = xml.getValue("RADIUS", 10);
    thin = xml.getValue("THIN", 10);
    scale = xml.getValue("SCALE", 10);

    bg_color = ofColor(xml.getValue("BG_COLOR:RED", 0),
                       xml.getValue("BG_COLOR:GREEN", 0),
                       xml.getValue("BG_COLOR:BLUE", 0));
    
    color = ofColor(xml.getValue("COLOR:RED", 255),
                    xml.getValue("COLOR:GREEN", 255),
                    xml.getValue("COLOR:BLUE", 255),
                    xml.getValue("COLOR:ALPHA", 255));
    
    outline_color = ofColor(xml.getValue("OUTLINE_COLOR:RED", 255),
                    xml.getValue("OUTLINE_COLOR:GREEN", 255),
                    xml.getValue("OUTLINE_COLOR:BLUE", 255),
                    xml.getValue("OUTLINE_COLOR:ALPHA", 255));
    
    line_color = ofColor(xml.getValue("LINE_COLOR:RED", 255),
                            xml.getValue("LINE_COLOR:GREEN", 255),
                            xml.getValue("LINE_COLOR:BLUE", 255),
                            xml.getValue("LINE_COLOR:ALPHA", 255));
    
    patterns.clear();
    int pattern_num = xml.getNumTags("PATTERN");
    for(int i = 0; i < pattern_num; i++){
        xml.pushTag("PATTERN", i);
            patterns.push_back(float());
            patterns[i * 4] = xml.getValue("M", 0);
            patterns.push_back(float());
            patterns[i * 4 + 1 ] = xml.getValue("N1", 0);
            patterns.push_back(float());
            patterns[i * 4 + 2 ] = xml.getValue("N2", 0);
            patterns.push_back(float());
            patterns[i * 4 + 3 ] = xml.getValue("N3", 0);
//        cout<<"xml.getValue(M, 0)"<<xml.getValue("M", 0)<<endl;
//        cout<<"xml.getValue(N1, 0)"<<xml.getValue("N1", 0)<<endl;
//        cout<<"xml.getValue(N2, 0)"<<xml.getValue("N2", 0)<<endl;
//        cout<<"xml.getValue(N3, 0)"<<xml.getValue("N3", 0)<<endl;

//            cout<<"patterns["<<i * 4<<"]"<<patterns[i * 4]<<endl;
//            cout<<"patterns["<<i * 4 + 1<<"]"<<patterns[i * 4 + 1]<<endl;
//            cout<<"patterns["<<i * 4 + 2<<"]"<<patterns[i * 4 + 2]<<endl;
//            cout<<"patterns["<<i * 4 + 3<<"]"<<patterns[i * 4 + 3]<<endl;
        xml.popTag();
    }
}

//--------------------------------------------------------------
void Scene_002::save(){
    ofxXmlSettings xml;
    xml.setValue("a", a);
    xml.setValue("b", b);
    xml.setValue("m1", m1);
    xml.setValue("m2", m2);
    xml.setValue("n1", n1);
    xml.setValue("n2", n2);
    xml.setValue("n3", n3);
    xml.setValue("RADIUS", radius);
    xml.setValue("THIN", thin);
    xml.setValue("SCALE", scale);

    xml.setValue("BG_COLOR:RED", bg_color->r);
    xml.setValue("BG_COLOR:GREEN", bg_color->g);
    xml.setValue("BG_COLOR:BLUE", bg_color->b);
    
    xml.setValue("COLOR:RED", color->r);
    xml.setValue("COLOR:GREEN", color->g);
    xml.setValue("COLOR:BLUE", color->b);
    xml.setValue("COLOR:ALPHA", color->a);
    
    xml.setValue("OUTLINE_COLOR:RED", outline_color->r);
    xml.setValue("OUTLINE_COLOR:GREEN", outline_color->g);
    xml.setValue("OUTLINE_COLOR:BLUE", outline_color->b);
    xml.setValue("OUTLINE_COLOR:ALPHA", outline_color->a);
    
    xml.setValue("LINE_COLOR:RED", line_color->r);
    xml.setValue("LINE_COLOR:GREEN", line_color->g);
    xml.setValue("LINE_COLOR:BLUE", line_color->b);
    xml.setValue("LINE_COLOR:ALPHA", line_color->a);
    
    for(int i = 0; i < patterns.size(); i+= 4){
        xml.addTag("PATTERN");
        xml.pushTag("PATTERN", i / 4);
        xml.addValue("M",  patterns[i]      );
        xml.addValue("N1", patterns[i + 1]  );
        xml.addValue("N2", patterns[i + 2]  );
        xml.addValue("N3", patterns[i + 3]  );
        xml.popTag();
    }

    
    xml.saveFile(xml_filename);
}
//--------------------------------------------------------------
void Scene_002::exit(){
}
//--------------------------------------------------------------
float Scene_002::superformula(float _theta, float _a, float _b, float _m1, float _m2, float _n1, float _n2, float _n3){
    float A = pow(ABS(cos(_m1 * _theta / 4) / _a), _n2);
    float B = pow(ABS(sin(_m2 * _theta / 4) / _b), _n3);
    return pow((A + B), -1/_n1);
}
//--------------------------------------------------------------
ofPoint Scene_002::pol2car(float _radius, float _theta){
    return ofPoint(cos(_theta) * _radius, sin(_theta) * _radius);
}



