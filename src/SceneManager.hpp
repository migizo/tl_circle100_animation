//
//  SceneManager.hpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//

#ifndef SceneManager_hpp
#define SceneManager_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "Animation.hpp"
#include "ofxGui.h"

#include "Scene_000.hpp"
#include "Scene_001.hpp"
#include "Scene_002.hpp"

class SceneManager{
    void setGui();
    
    //xml
    ofxXmlSettings xml;
    string xml_filename = "xml/config.xml";
    
    //scene
    vector<BaseScene *> scenes;
    vector<int> order;
    int current = 0;
    int last;
    
    //fade
    void startScene();
    ofParameter<float> fade_sec;
    Animation_Sec fade;
    Animation_Sec delayTask;
    
    ofxPanel gui;
    
    bool isInit = false;
public:
    void setup();
    void update();
    void draw();
    void load();
    void save();
    void exit();
    void changeScene();
    void testMethod();
    ofSoundBuffer audioOut(ofSoundBuffer outBuffer);
    //gui
    bool isDrawGui = false;
};

#endif /* SceneManager_hpp */
