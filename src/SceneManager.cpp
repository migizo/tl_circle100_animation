//
//  SceneManager.cpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//

#include "SceneManager.hpp"
void SceneManager::setup(){
    scenes.push_back(new Scene_000());
    scenes.push_back(new Scene_001());
    scenes.push_back(new Scene_002());
    
    for(int i = 0; i < scenes.size(); i++){
        scenes[i]->setup();
    }
    load();
    
    ofAddListener(delayTask.specified_event, this, &SceneManager::changeScene);
    ofAddListener(fade.end_event, this, &SceneManager::startScene);
    
    startScene();
    isInit = true;
}

//--------------------------------------------------------------
void SceneManager::update(){
    ofSetBackgroundAuto(true);

    fade.update();
    delayTask.update();
    scenes[order[current]]->update();

}

//--------------------------------------------------------------
void SceneManager::draw(){
    if(fade.isAnimation){
        scenes[order[current]]->setAlpha(fade.getNormalize() * 255);
        scenes[order[current]]->draw();
        
        scenes[order[last]]->setAlpha((1 - fade.getNormalize()) * 255);
        scenes[order[last]]->draw();
    }
    else{
        scenes[order[current]]->draw();
    }
    if(isDrawGui){
        gui.draw();
        ofPushStyle();
        ofSetColor(255, 100, 100, 255);
        ofDrawBitmapString(ofToString(ofGetFrameRate()), 20, ofGetHeight() - 40);
        ofPopStyle();
    }

}

//--------------------------------------------------------------
void SceneManager::load(){
    xml.loadFile(xml_filename);
    int order_num = xml.getNumTags("ORDER");
    order.clear();
    for(int i = 0; i < order_num; i++){
        order.push_back(int());
        order[i] = xml.getValue("ORDER", 0, i);
    }
    fade_sec = xml.getValue("FADE_SEC", 0.5);
    
    int total_sec_num = xml.getNumTags("TOTAL_SEC");
    for(int i = 0; i < total_sec_num; i++){
        scenes[i]->total_sec = xml.getValue("TOTAL_SEC", 10, i);
    }
    
    //各シーンのパラメータ読み込み処理
    for(int i = 0; i < scenes.size(); i++){
        scenes[i]->load();
    }
}
//--------------------------------------------------------------
void SceneManager::save(){
    for(int i = 0; i < order.size(); i++){
        xml.setValue("ORDER", order[i], i);
    }
    xml.setValue("FADE_SEC", fade_sec);
    int total_sec_num = xml.getNumTags("TOTAL_SEC");
    for(int i = 0; i < total_sec_num; i++){
        xml.setValue("TOTAL_SEC", scenes[i]->total_sec, i);
    }
    
    xml.saveFile(xml_filename);
    //各シーンのパラメータ保存
    for(int i = 0; i < scenes.size(); i++){
        scenes[i]->save();
    }
}

//--------------------------------------------------------------
void SceneManager::setGui(){
    gui.clear();
    
    gui.setup();
    gui.add(fade_sec.set("fade_sec", fade_sec, 0.1, 2.0));
    gui.setName("settings");
    gui.add(scenes[order[current]]->total_sec.set("total_sec", scenes[order[current]]->total_sec, 3, 15));
    gui.add(scenes[order[current]]->params);
}

//--------------------------------------------------------------
void SceneManager::changeScene(){
    last = current;
    current++;
    current = current < order.size() ? current : 0;
    
    fade.start(fade_sec);
}
//--------------------------------------------------------------
void SceneManager::startScene(){
    setGui();
    float fadestart_sec = scenes[order[current]]->total_sec - fade_sec;
    delayTask.setSpecifiedSec(fadestart_sec);//クロスフェードを開始するタイミング
    delayTask.start(scenes[order[current]]->total_sec);//アニメーションの所要時間を設定
}
//--------------------------------------------------------------
void SceneManager::testMethod(){
    scenes[order[current]]->test();
}
//--------------------------------------------------------------
ofSoundBuffer SceneManager::audioOut(ofSoundBuffer outBuffer){
    ofSoundBuffer buf = outBuffer;

    if(isInit){
        if(scenes.size() > 0){
            cout<<"current "<<current << endl;
            cout<<"order[current] "<<order[current] << endl;
            
            buf = scenes[order[current]]->audioOut(buf);
        }
    }
   
    return buf;
}
//--------------------------------------------------------------
void SceneManager::exit(){
        scenes[order[current]]->exit();
}
