//
//  Scene_000.cpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//

#include "Scene_000.hpp"
void Scene_000::setup(){
    //SoundManagerのセットアップ & 鳴らす音数の指定
    soundManager.setup();
    soundManager.addMultiSounds(TRACK_NUM);
    
    //BPMと各トラックにシーケンスパターンを指定
    pattern[0] = 0b1000100010001000;
    pattern[1] = 0b1010101010101010;
    pattern[2] = 0b0000100000001000;
    pattern[3] = 0b1010101010101010;
    pattern[4] = 0b1011101101101000;
    pattern[5] = 0b1000000010000000;
    pattern[6] = 0b1010101010101010;
    
    //音が鳴らされていない状態に初期化
    for(int i = 0; i < TRACK_NUM; i++){
        isPlay[i] = false;
    }
    
    //GUIにパラメータを設定
    setParams();
}
//--------------------------------------------------------------

void Scene_000::update(){
    //SoundManagerの更新
    soundManager.update();

    updateNote();
    
    //16分のタイミング毎にパターンをチェックして打音のタイミングの場合音を鳴らす。音程はランダムに設定
    float sec = fmodf(ofGetElapsedTimef(), beat16_by_bpm(bpm));
    if( sec < last_sec){
        for(int i = 0; i < TRACK_NUM; i++){
            if(checkPattern(i, counter)){
                if(i == 0) playNote(i, key);
                else playNote(i, note_ratio[(int)ofRandom(5)] * key);
            }
        }
        counter++;
        counter = counter > TRACK_CIRCLE_NUM ? 0 : counter;
    }
    last_sec = sec;
}

//--------------------------------------------------------------
void Scene_000::draw(){
    //背景の描画
    ofSetColor(bg_color, alpha);
    ofDrawRectangle(0, 0, SCREEN_W, SCREEN_H);
    
    //背景の線の描画
    ofColor bg_line_color = bg_color;
    bg_line_color = bg_line_color.invert();
    ofSetColor(bg_line_color, alpha - diff_bg_line_alpha);
    bg_line.draw();
    
    //ヘッダー部分の回転する4つの玉の描画
    ofSetColor(header_cicle_color, alpha);
    for(int i = 0; i < 4; i++){
        ofPushMatrix();
        float _x = (float)ofGetWidth() / 2 - (header_marginX * 1.5 + large_radius * 2) + (large_radius + header_marginX) * i;
        float _y =SCREEN_H/ 2 - TRACK_H * 3 - TRACK_H / 2;
        ofTranslate(_x, _y);
        int threshold = SCREEN_W / 8;
        ofDrawCircle(0, 0, ofMap((getX(16 - (i * 4)) < threshold) * soundManager.sounds[0].volume, 0, 1, small_radius, large_radius));
        ofPopMatrix();
    }
    
    //6つのトラックの描画
    for(int i = 1; i < TRACK_NUM; i++){
        ofPushMatrix();
        ofTranslate(0, SCREEN_H/ 2 - TRACK_H * 3 + TRACK_H * (i -1));
        for(int j = 0; j < TRACK_CIRCLE_NUM; j++){
            float _radius = checkPattern(i, j) ? large_radius : small_radius;
            ofColor color;
            color.setHsb(fmodf(bg_line_color.getHue() + (255/3)/TRACK_NUM * i, 255), 180, 255);
            ofSetColor(color, alpha);
            line[i].draw();
            ofDrawCircle(getX(j) , TRACK_H * 0.5, _radius);
        }
        ofPopMatrix();
    }
    
}
//--------------------------------------------------------------
void Scene_000::setParams(){
    params.add(bpm.set("BPM", bpm, 50, 250));
    params.add(small_radius.set("small_radius", small_radius, 10, TRACK_H));
    params.add(large_radius.set("large_radius", large_radius, 10, TRACK_H));
    params.add(header_marginX.set("header_marginX", header_marginX, 10, 200));
    params.add(threshold_bg_line.set("threshold_bg_line", threshold_bg_line, 100, 1500));
    params.add(diff_bg_line_alpha.set("diff_bg_line_alpha", diff_bg_line_alpha, 0, 255));
    params.add(bg_color.set("bg_color", bg_color, ofColor(0), ofColor(255)));
    params.add(header_cicle_color.set("header_cicle_color", header_cicle_color, ofColor(0), ofColor(255)));

}

//--------------------------------------------------------------
void Scene_000::load(){
    ofxXmlSettings xml;
    xml.loadFile(xml_filename);
    bpm = xml.getValue("BPM", bpm);
    small_radius = xml.getValue("SMALL_RADIUS", small_radius);
    large_radius = xml.getValue("LARGE_RADIUS", large_radius);
    header_marginX = xml.getValue("HEADER_MARGIN_X", header_marginX);
    threshold_bg_line = xml.getValue("THRESHOLD_BG_LINE", threshold_bg_line);
    diff_bg_line_alpha = xml.getValue("DIFF_BG_LINE_ALPHA", diff_bg_line_alpha);
    bg_color = ofColor(xml.getValue("BG_COLOR:RED", bg_color->r),
                       xml.getValue("BG_COLOR:GREEN", bg_color->g),
                       xml.getValue("BG_COLOR:BLUE", bg_color->b));
    header_cicle_color = ofColor(xml.getValue("HEADER_CIRCLE_COLOR:RED", header_cicle_color->r),
                                 xml.getValue("HEADER_CIRCLE_COLOR:GREEN", header_cicle_color->g),
                                 xml.getValue("HEADER_CIRCLE_COLOR:BLUE", header_cicle_color->b));

}
//--------------------------------------------------------------
void Scene_000::save(){
    ofxXmlSettings xml;
    xml.setValue("BPM", bpm);
    xml.setValue("SMALL_RADIUS", small_radius);
    xml.setValue("LARGE_RADIUS", large_radius);
    xml.setValue("HEADER_MARGIN_X", header_marginX);
    xml.setValue("THRESHOLD_BG_LINE", threshold_bg_line);
    xml.setValue("DIFF_BG_LINE_ALPHA", diff_bg_line_alpha);
    xml.setValue("BG_COLOR:RED", bg_color->r);
    xml.setValue("BG_COLOR:GREEN", bg_color->g);
    xml.setValue("BG_COLOR:BLUE", bg_color->b);
    xml.setValue("HEADER_CIRCLE_COLOR:RED", header_cicle_color->r);
    xml.setValue("HEADER_CIRCLE_COLOR:GREEN", header_cicle_color->g);
    xml.setValue("HEADER_CIRCLE_COLOR:BLUE", header_cicle_color->b);
    
    xml.saveFile(xml_filename);

}
//--------------------------------------------------------------
void Scene_000::exit(){
    soundManager.exit();
}
//-------------------------------------------------------------
ofSoundBuffer Scene_000::audioOut(ofSoundBuffer outBuffer){
//    ofScopedLock soundLock(soundMutex);
    ofSoundBuffer buf = outBuffer;
    //SoundManager内で音声処理
    buf = soundManager.autioOut(buf);
    
    //line
    for(int h = 1; h < TRACK_NUM; h++){
        line[h].clear();
        for(int i = 0; i < soundManager.sounds[h].buffer.size(); i+= soundManager.channel_num){
            line[h].addVertex(ofMap(i, 0, soundManager.sounds[h].buffer.size() - 1, 0, ofGetWidth()),
                              ofMap(soundManager.sounds[h].buffer[i], -1, 1, TRACK_H , 0, true));
        }
    }
    
    //=============
    //TODO
    //SCREEN_Hに値を足して調整している部分を分布を変更して調整する
    //Easing補間関数など使用する
    //=============
    //bg_line
    counter_bg_line++;
    if(counter_bg_line > threshold_bg_line){
        counter_bg_line = 0;
        bg_line.clear();
    }
    for(int i = 0; i < buf.size(); i++){
        bg_line.addVertex(ofMap(i, 0, buf.size() - 1, 0, ofGetWidth()),
                          ofMap(buf[i], -1, 1, SCREEN_H + 500, -500));
    }
    return buf;

}

//-------------------------------------------------------------
//スクリーン上に均等に16つ並べるための位置を取得
// (16分音符 * トラック数)を足してトラック毎に異なる位置になるように。 1から引くことで反転
float Scene_000::getX(int _id, int _offset){
    float track_time = ofGetElapsedTimef() + beat16_by_bpm(bpm) * (_id + _offset);
    float calcModX =fmodf(track_time, bar1_by_bpm(bpm));
    return (1.0f - calcModX / bar1_by_bpm(bpm)) * SCREEN_W;
}

//-------------------------------------------------------------
bool Scene_000::checkPattern(int track_id, int shift){
    int bit_mask = 0b0000000000000001;
    bit_mask = bit_mask << shift;
    return pattern[track_id] & bit_mask;
 }

//-------------------------------------------------------------
int Scene_000::playNote(int track_id, int _note){
    isPlay[track_id] = true;
    soundManager.sounds[track_id].hz_target = _note;
}

//-------------------------------------------------------------
void Scene_000::updateNote(){
    for(int i = 0; i < TRACK_NUM; i++){
        if(isPlay[i]){
            //音を鳴らす(音量を瞬時に大きくする)
            soundManager.sounds[i].volume = ofLerp(soundManager.sounds[i].volume , 1, 0.8);
            soundManager.sounds[i].hz = ofLerp(soundManager.sounds[i].hz_target , 1, 0.8);
            if(soundManager.sounds[i].volume  > 0.9)isPlay[i] = false;
        }
        else{
            //音量をゆっくり小さくする
            soundManager.sounds[i].volume  = ofLerp(soundManager.sounds[i].volume , 0, 0.1);
            soundManager.sounds[i].hz  = ofLerp(soundManager.sounds[i].hz_target , 0, 0.1);
            
        }
    }
}

