//
//  Scene_002.hpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//
/*
 シーン概要
superformula
 以下を参考に実装しました。
superformula https://en.wikipedia.org/wiki/Superformula
知人から入手したHoudini VEXのチュートリアル
 */
#ifndef Scene_002_hpp
#define Scene_002_hpp

#include <stdio.h>
#include "BaseScene.hpp"

//#define PATTERN_NUM 12
//#define PETTERN_ELEMENT_NUM 4

class Scene_002 : public BaseScene{
private:
    vector<float> patterns;
//    float patterns[PATTERN_NUM][PETTERN_ELEMENT_NUM] = {
//        {3, 4.5, 10, 10},
//        {4, 12, 15, 15},
//        {7, 10, 6, 6},
//        {5, 4, 4, 4},
//        {5, 2, 7, 7},
//        {5, 2, 13, 13},
//        {4, 1, 1, 1},
//        {4, 1, 7, 8},
//        {6, 1, 7, 8},
//        {2, 2, 2, 2},
//        {1, 0.5, 0.5, 0.5},
//        {2, 0.5, 0.5, 0.5}
//    };
    int current_pattern_idx = 0;
    string xml_filename = "xml/scene002.xml";
    
    //時計表示用
    string font_filename = "font/vag.ttf";
    ofTrueTypeFont font;
    
    
    void setParams();
    ofParameter<float> a;
    ofParameter<float> b;
    ofParameter<float> m1;
    ofParameter<float> m2;
    ofParameter<float> n1;
    ofParameter<float> n2;
    ofParameter<float> n3;
    ofParameter<float> radius;
    ofParameter<float> thin;//厚さ
    ofParameter<float> scale;//全体の範囲、サイズ


    ofParameter<ofColor> bg_color;
    ofParameter<ofColor> color;
    ofParameter<ofColor> outline_color;
    ofParameter<ofColor> line_color;

    ofEasyCam cam;
    float superformula(float _theta, float _a, float _b, float _m1, float _m2, float _n1, float _n2, float _n3);
    
    //極座標->デカルト座標に変換
    ofPoint pol2car(float _radius, float _theta);
public:
    void setup();
    void update();
    void draw();
    void load();
    void save();
    void exit();
    void test(){};
    ofSoundBuffer audioOut(ofSoundBuffer outBuffer){
//        ofScopedLock soundLock(soundMutex);
        ofSoundBuffer buf = outBuffer;
        return buf;
    };
};
#endif /* Scene_002_hpp */
