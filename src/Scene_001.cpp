//
//  Scene_001.cpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//

#include "Scene_001.hpp"
void Scene_001::setup(){
    setParams();
    for(int i = 0; i < CIRCLE_NUM; i++){
        circle.push_back(BoidCircle());
    }
}

//--------------------------------------------------------------
void Scene_001::update(){
    //Boidsの更新
    for(int i = 0; i < CIRCLE_NUM; i++){
        circle[i].setMaxSpeed(max_speed);
        circle[i].setMaxForce(max_force);
        circle[i].setRadius(radius);
        circle[i].flock(circle);
        circle[i].update();
    }
}

//--------------------------------------------------------------
void Scene_001::draw(){
    color *= ofColor(255, alpha);//GUIで設定しているalphaをフェード用alphaで乗算
    
    //bg
    ofSetColor(bg_color);
    ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    
    ofSetColor(color);
    for(int i = 0; i < CIRCLE_NUM; i++){
        circle[i].draw();
    }

}
//--------------------------------------------------------------
void Scene_001::setParams(){
    params.add(color.set("color", ofColor(255, 255), ofColor(0, 0), ofColor(255)));
    params.add(bg_color.set("bg_color", ofColor(0, 255), ofColor(0, 0), ofColor(255)));
    params.add(max_force.set("max_force", 0.03f, 0.01, 0.1));
    params.add(max_speed.set("max_speed", 2, 1, 8));//最大速度
    params.add(radius.set("radius", 10, 1, 200));//半径
}
//--------------------------------------------------------------
void Scene_001::load(){
    ofxXmlSettings xml;
    xml.loadFile(xml_filename);
    max_force = xml.getValue("MAX_FORCE", 0.03f);
    max_speed = xml.getValue("MAX_SPEED", 2);
    radius = xml.getValue("RADIUS", 10);
    color = ofColor(xml.getValue("COLOR:RED", 255),
                    xml.getValue("COLOR:GREEN", 255),
                    xml.getValue("COLOR:BLUE", 255),
                    xml.getValue("COLOR:ALPHA", 255));

    bg_color = ofColor(xml.getValue("BG_COLOR:RED", 0),
                       xml.getValue("BG_COLOR:GREEN", 0),
                       xml.getValue("BG_COLOR:BLUE", 0));

}
//--------------------------------------------------------------
void Scene_001::save(){
    ofxXmlSettings xml;
    xml.setValue("MAX_FORCE", max_force);
    xml.setValue("MAX_SPEED", max_speed);
    xml.setValue("RADIUS", radius);
    
    xml.setValue("COLOR:RED", color->r);
    xml.setValue("COLOR:GREEN", color->g);
    xml.setValue("COLOR:BLUE", color->b);
    xml.setValue("COLOR:ALPHA", color->a);

    xml.setValue("BG_COLOR:RED", bg_color->r);
    xml.setValue("BG_COLOR:GREEN", bg_color->g);
    xml.setValue("BG_COLOR:BLUE", bg_color->b);

    xml.saveFile(xml_filename);
}
//--------------------------------------------------------------
void Scene_001::exit(){
}

