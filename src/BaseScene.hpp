//
//  BaseScene.hpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//

#ifndef BaseScene_hpp
#define BaseScene_hpp

#include <stdio.h>
#include "ofMain.h"
//#include "Animation.hpp"
#include "ofxXmlSettings.h"
#define SCREEN_W 1920
#define SCREEN_H 1080

class BaseScene{
protected:
    float alpha = 255.0;
    virtual void setParams(){}
//    ofMutex soundMutex;
public:
    ofParameter<float> total_sec;
    ofParameterGroup params;
    virtual void setup(){
        
    }
    virtual void update(){
        
    }
    virtual void draw(){
        
    }
    virtual void load(){
        
    }
    virtual void save(){
        
    }
    virtual void test(){
        
    }
    virtual void exit(){
        
    }
    virtual ofSoundBuffer audioOut(ofSoundBuffer outBuffer){
    }
    
    void setAlpha(float alpha){
        this->alpha = alpha;
    }
};

#endif /* BaseScene_hpp */
