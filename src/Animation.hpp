//
//  Animation.hpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/05/26.
//

#ifndef Animation_hpp
#define Animation_hpp

#include <stdio.h>
#include "ofMain.h"
class Animation_Frame{
    int specified_frame;
    bool isCheckSpecified = false;
public:
    int total_frame;
    ofEvent<void> end_event;
    ofEvent<void> specified_event;
    ofParameter<int> current_frame = 0;
    bool isAnimation = false;
    
    void start(int total_frame){
        this->total_frame = total_frame;
        current_frame = 0;
        isAnimation = true;
    }
    void setSpecifiedSec(int specified_frame){
        this->specified_frame = specified_frame;
        isCheckSpecified = true;
    }
    
    void update(){
        if(isAnimation){
            current_frame++;
            if(isCheckSpecified && current_frame == specified_frame){
                ofNotifyEvent(specified_event);
                isCheckSpecified = false;
            }
            if(current_frame == total_frame){
                ofNotifyEvent(end_event);
                isAnimation = false;
            }
        }
    }
};

class Animation_Sec{
    float specified_sec;
    bool isCheckSpecified = false;
public:
    float total_sec;
    ofEvent<void> end_event;
    ofEvent<void> specified_event;
    float start_sec = 0;
    float current_sec = 0;
    bool isAnimation = false;
    
    void start(float total_sec){
        isAnimation = true;
        this->total_sec = total_sec;
        start_sec = ofGetElapsedTimef();
        current_sec = 0;
    }
    void setSpecifiedSec(float specified_sec){
        this->specified_sec = specified_sec;
        isCheckSpecified = true;
    }
    
    void update(){
        if(isAnimation){

            current_sec = ofGetElapsedTimef() - start_sec;
            
            if(isCheckSpecified && current_sec > specified_sec){
                ofNotifyEvent(specified_event);
                isCheckSpecified = false;
            }
            else if(current_sec > total_sec){
                ofNotifyEvent(end_event, this);
                isAnimation = false;
            }
        }
    }
    float getNormalize(){
        return current_sec / total_sec;
    }
};

#endif /* Animation_hpp */

