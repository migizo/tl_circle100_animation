//
//  Scene_001.hpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//
/*
 シーン概要
 群(Boids)シミュレーション
 
 以下を参考に実装しました。
processingコード https://processing.org/examples/flocking.html
NATURE OF CODE http://amzn.asia/db8l1Xa
 */
#ifndef Scene_001_hpp
#define Scene_001_hpp

#include "BaseScene.hpp"
#include "BoidCircle.h"

#include <stdio.h>

#define CIRCLE_NUM 100
class Scene_001 : public BaseScene{
    //設定ファイル
    string xml_filename = "xml/scene001.xml";
    
    //GUI パラメータの設定する関数
    void setParams();
    ofParameter<ofColor> bg_color;      //背景色
    ofParameter<ofColor> color;         //円の色
    ofParameter<float> max_force;       //最大運動量
    ofParameter<float> max_speed;       //最大速度
    ofParameter<float> radius;          //半径
public:
    void setup();
    void update();
    void draw();
    void load();
    void save();
    void exit();
    void test(){};
    ofSoundBuffer audioOut(ofSoundBuffer outBuffer){
//        ofScopedLock soundLock(soundMutex);
        ofSoundBuffer buf = outBuffer;
        return buf;
    };
    
    //群シミュレーション(Boids)の円のインスタンス
    vector<BoidCircle> circle;
};

#endif /* Scene_001_hpp */
