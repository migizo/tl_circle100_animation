//
//  Scene_000.hpp
//  tl_circle100_animation
//
//  Created by 浅野昇平 on 2018/06/02.
//
/*
 シーン概要
 サイン波のシーケンサー
 16個(=16分音符)円が流れるトラックが6行と
 4つ打ちのタイミングで半径が変わる円が4つ
 (16 * 6 + 4 = 100)
 
 二進数のビット配列で音を鳴らすタイミングを指定しています。
 音はofSoundBufferベースで660Hz( = E,ミ)の平均律の四七抜き音階でリアルタイムでメロディパターンをランダムに生成しています。
 
 バッファデータに格納されている値をofPolylineに適用して、複雑な線で糸のような質感を目指しました。
 */
#ifndef Scene_000_hpp
#define Scene_000_hpp

#include <stdio.h>
#include "BaseScene.hpp"
#include "Sound.h"
#define TRACK_H 120     //各トラックの高さ(px)
#define TRACK_NUM 7     //6つのトラック + 4つ打ち用
#define TRACK_CIRCLE_NUM 16   //1つのトラックに表示する円の数
//BPMから各音価の時間を計算
static float beat4_by_bpm(float bpm){return 60.0f / bpm;}                       //4分音符
static float beat16_by_bpm(float bpm){return beat4_by_bpm(bpm) * 0.25f;}        //16分音符
static float beat8_by_bpm(float bpm){return beat4_by_bpm(bpm) * 0.5f;}          //8分音符
static float bar1_by_bpm(float bpm){return beat4_by_bpm(bpm) * 4;}              //1小節

class Scene_000 : public BaseScene{
private:
    string xml_filename = "xml/scene000.xml";
    void setParams();                           //gui groups設定
    ofParameter<float> bpm = 72;
    ofParameter<float> key = 440;               //基音の周波数
    ofParameter<int> threshold_bg_line = 1000;  //背景の線をリセットするタイミング
    ofParameter<float> diff_bg_line_alpha = 200;//背景の線の透明度
    ofParameter<float> large_radius = 40;
    ofParameter<float> small_radius = 15;
    ofParameter<float> header_marginX = 130;    //ヘッダーの円が並ぶ間隔
    ofParameter<ofColor> bg_color;              //背景の色
    ofParameter<ofColor> header_cicle_color;    //ヘッダーの円の色
    
    
    //それぞれの音律はwikipediaを参考にしました
    //https://ja.wikipedia.org/wiki/%E5%B9%B3%E5%9D%87%E5%BE%8B
    float note_ratio[5] = {
        1,          //C ド
        1.122462,   //D レ
        1.259921,   //E ミ
        1.498307,   //G ソ
        1.681793    //A ラ
    };
    
    float last_sec;                                             //シーケンスの処置で使用。1フレーム前のsecを取得
    int  counter = 0;                                           //pattern変数をループで読み込むためのカウンター (ex. counter % 16 )
    int pattern[TRACK_NUM];                                     //音を鳴らすタイミング(16分音符で指定)
    bool isPlay[TRACK_NUM];                                     //音を鳴らしているかどうか
    
    ofPolyline line[TRACK_NUM];                                 //各トラック毎に描画している線
    ofPolyline bg_line;                                         //背景に大きく描画している線
    int counter_bg_line = 0;                                    //背景に大きく描画している線を定期的にリセットするためのカウンター
    
    SoundManager soundManager;                                  //SoundManagerクラスのインスタンス
    
    bool checkPattern(int track_id, int shift);                 //指定した16分(音符)のタイミングでシーケンスパターンがあればtrueを返す
    int playNote(int track_id, int _note);                      //指定した音を鳴らす
    void updateNote();                                          //音が鳴らされているかチェック、音量と周波数をスムーズに遷移させる
    float getX(int _id, int _offset = 0);                       //スクリーン上に均等に16つ並べるための位置を取得

public:
    void setup();
    void update();
    void draw();
    void load();
    void save();
    void exit();
    void test(){};
    ofSoundBuffer audioOut(ofSoundBuffer outBuffer);
    
};
#endif /* Scene_000_hpp */
